#!/bin/python

# Copyright © 2020 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import influxdb_client
import gitlab
import pytz
import requests
import json
import logging

from dateutil import parser
from datetime import datetime

from influxdb_client.client.write_api import SYNCHRONOUS, WriteApi
from influxdb_client.client.query_api import QueryApi
from influxdb_client.client.delete_api import DeleteApi

from bz2 import decompress

import settings

class Database:
    """Simple wrapper around InfluxDBClient with helper functions specific for this script"""
    __slots__ = ('client', 'query_api', 'write_api', 'delete_api')

    def __init__(self):
        self.client = influxdb_client.InfluxDBClient(
            url=settings.INFLUX_URL,
            token=settings.INFLUX_TOKEN,
            org=settings.INFLUX_ORG
        )
        self.query_api = self.client.query_api()
        self.write_api = self.client.write_api()
        self.delete_api = self.client.delete_api()

    def __del__(self):
        self.write_api.flush()

    def write_frame_time(self, pipeline_time, job_name, trace_name, frame_time):
        """- `trace_name` Name of the trace that has been replayed
        - `frame_time` Time for a frame to hit presentation in nanoseconds"""
        p = influxdb_client.Point("frame_time").tag("job_name", job_name).tag("trace_name", trace_name).field("frame_time", frame_time).time(pipeline_time)
        self.write_api.write(bucket=settings.INFLUX_BUCKET, org=settings.INFLUX_ORG, record=p)

    def test_delete(self):
        """Currently does not work. Drop and recreate bucket instead."""
        # @todo It could be fixed now. Update influxdb and check again.
        start = "1970-01-01T00:00:00.000000001Z"
        stop = "2121-01-01T00:00:00.000000001Z"
        self.delete_api.delete(start, stop, '_field="frame_time"', settings.INFLUX_BUCKET, settings.INFLUX_ORG)

    def query_last_frame_time_datetime(self):
        """- `return` The datetime of the last frame time written in the bucket"""
        # Relative durations used for a range of time, expect for the last element
        # which is an absolute Unix timestamp used just to get all entries
        duration_literals = ['-1h', '-1d', '-1w', '-1mo', '-1y', '0']

        # Query DB expanding the range until we find an element
        query_template = ' from(bucket: "{}") |> range(start: {}) |> filter(fn:(r) => r._measurement == "frame_time") |> last()'
        for literal in duration_literals:
            query = query_template.format(settings.INFLUX_BUCKET, literal)
            result = self.query_api.query(query, org=settings.INFLUX_ORG)
            if len(result) > 0:
                break

        # No records available, return epoch
        if len(result) == 0:
            logging.info("No records found in DB, returning epoch")
            # In this context epoc represents the time when frame time results were introduced the first time
            epoc = datetime(2020, 12, 15, tzinfo=pytz.UTC)
            return epoc

        # Get time of last record
        last_date = result[0].records[0].get_time()
        logging.info("Last record datetime in DB: {}".format(last_date))
        return last_date

class Project:
    """Simple class aroung GitlabProject with helper functions specific for this script"""
    __slots__ = ('path', 'project')

    def __init__(self):
        gl = gitlab.Gitlab(settings.GITLAB_URL, private_token=settings.GITLAB_TOKEN)
        self.path = settings.GITLAB_PROJECT_PATH
        self.project = gl.projects.get(self.path)

    def get_pipelines_newer_than(self, date_from: datetime):
        """- `date_from` A datetime from which to start looking for new pipelines
        - `return` A list of pipelines of this project that are newer than the specified datetime"""
        logging.info("Looking for pipelines newer than {}".format(date_from))

        pipelines = []

        pipelines_gen = self.project.pipelines.list(as_list=False, per_page=100)

        for p in pipelines_gen:
            p_date = parser.parse(p.created_at)
            if p_date <= date_from:
                break
            pipelines.append(p)

        if len(pipelines) == 0:
            logging.info("No new pipelines found")
        else:
            logging.info("Found {} new pipelines".format(len(pipelines)))

        return pipelines

class Results:
    """A class representing job results containing frametimes out our traces replays"""

    def __init__(self, results):
        self.results = results

    def apitrace_traces_iter(self):
        """- `return` A generator list of apitrace traces names"""
        return (trace for trace in self.results['tests'] if trace.endswith(".trace"))

    def get_trace_frame_times(self, trace: str):
        """- `return` The frame times of a specified trace within the specified results"""
        frame_times = []

        print(self.results['tests'][trace]['images'])
        if "frame_times" in self.results['tests'][trace]['images'][0]:
            frame_times = self.results['tests'][trace]['images'][0]["frame_times"]
            logging.info("Found {} frame times in {}".format(len(frame_times), trace))

        return frame_times

class Job:
    """A simplified version of the Gitlab Job class"""

    __slots__ = ('pipeline', 'id', 'name')

    def __init__(self, pipeline, id, name):
        self.pipeline = pipeline
        self.id = id
        self.name = name

    def get_results(self):
        """- `return` A Results object with the results artifacts of the specified job"""
        if self.pipeline.status == 'running':
            logging.warning("Pipeline {} still running".format(self.pipeline.id))
            return None

        url = "{}/{}/-/jobs/{}/artifacts/raw/results/results.json.bz2".format(
            settings.GITLAB_URL,
            settings.GITLAB_PROJECT_PATH,
            self.id
        )
        logging.info("Requesting results from: {}".format(url))
        r = requests.get(url)

        if r.status_code != 200:
            logging.error("Failed to download ({}):\n\t{}".format(
                    r.status_code,
                    url
                )
            )
            return None

        return Results(json.loads(decompress(r.content)))

def get_trace_jobs(pipeline):
    """ - `pipeline` The pipeline from which to look for trace jobs
    - `return` The jobs of the pipeline responsible for replaying traces"""
    jobs = []

    jobs_skipped = 0

    for gitlab_job in pipeline.jobs.list(all=True):
        is_trace_job = "-profile-traces" in gitlab_job.name
        if is_trace_job:
            # Some failed jobs still manage to replay traces and store the results artifact
            success = (gitlab_job.status == "success" or gitlab_job.status == "failed")
            if success:
                jobs.append(Job(pipeline, gitlab_job.id, gitlab_job.name))
            else:
                jobs_skipped += 1

    logging.info("{} jobs skipped".format(jobs_skipped))

    return jobs
