#!/bin/python

# Copyright © 2020 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab_influx as gi
import logging
import argparse
import sys

from datetime import timedelta
from dateutil import parser

def run():
    database = gi.Database()
    date_from = database.query_last_frame_time_datetime()

    project = gi.Project()
    pipelines = project.get_pipelines_newer_than(date_from)

    for pipeline in pipelines:
        pipeline_time = parser.parse(pipeline.created_at)
        logging.info("Processing pipeline created at {}".format(pipeline_time))

        trace_jobs = gi.get_trace_jobs(pipeline)
        for job in trace_jobs:
            results = job.get_results()

            # Skip jobs with no results
            if results is None:
                continue

            # Get frame times for apitrace traces
            for trace in results.apitrace_traces_iter():
                frame_times = results.get_trace_frame_times(trace)
                # Ignore first, second and last frame
                # First frame is the setup frame, the second frame is the first iteration
                for frame_num, frame_time in enumerate(frame_times[2:-1]):
                    frame_time = int(frame_time)
                    sample_time = pipeline_time + timedelta(milliseconds=frame_num)
                    # Update influxDB with current trace
                    database.write_frame_time(sample_time, job.name, trace, frame_time)

def main(args):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--log',
        required=False,
        default='WARNING',
        help='the log level',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
    )

    args = parser.parse_args(args)

    # Set logging level
    logging.basicConfig(level=getattr(logging, args.log.upper()))

    return run()

if __name__ == '__main__':
    all_ok = main(sys.argv[1:])
    sys.exit(0 if all_ok else 1)
