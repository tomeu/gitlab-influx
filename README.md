# Collecting data from GitLab

By just running `python main.py` you can start collecting data from Mesa CI pipelines which will be stored in the `mesa` InfluxDB bucket.
The script works by performing a query to InfluxDB for the latest element in the bucket, and will only collect results from pipelines which are newer than that.
This means that by running the script periodically you can keep the database synchronized with the state of Mesa CI.

## Prerequisites

1. Install InfluxDB and start it as a service.

2. Go to `localhost:8086` and set up your user, organization and `mesa` bucket.

3. Login and create a token with read access to `mesa` bucket.

## Grafana

1. Install Grafana and start it as a service.

2. Go to grafana->configuration->datasource and add an InfluxDB, making sure to use the correct organization name and token just created.

3. Then create->dashboard->panel and select InfluxDB in the query tab.

4. A starting query could be:
   ```
   from(bucket:"mesa")
       |> range(start:-30d)
   ```
